package mx.com.gm.rest;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.gm.domain.Materias;
import mx.com.gm.service.MateriasService;

@RestController
@RequestMapping("v1/materias")
public class RestMateriasController {

	@Autowired
	private MateriasService materiasService;
	
	@GetMapping
	public List<Materias> getAll(){
		return materiasService.listarAlumnos();
	}
	
	@PostMapping
	public ResponseEntity<Materias> registrar(@Valid @RequestBody Materias materias) {
		return new ResponseEntity<Materias>(materiasService.registrar(materias), HttpStatus.CREATED);
	}
}
