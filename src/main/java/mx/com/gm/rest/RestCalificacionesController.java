package mx.com.gm.rest;

import java.util.List;

import javax.validation.Valid;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.gm.domain.Calificaciones;
import mx.com.gm.service.CalificacionesService;

@RestController
@RequestMapping("v1/calificaciones")
public class RestCalificacionesController {
	
	@Autowired
	private CalificacionesService calificacionesService;
	
	@GetMapping
	public List<Calificaciones> getAll(){
		return calificacionesService.listarCalificaciones();
	}
	
//	@GetMapping("{/id}")
//	public ResponseEntity<Calificaciones> getAll(){
//		return calificacionesService.
//	}
	
	@PostMapping
	public ResponseEntity<Calificaciones> registrar(@Valid @RequestBody Calificaciones calificaciones) {
		return new ResponseEntity<>(calificacionesService.registrar(calificaciones), HttpStatus.CREATED);
	}
	
	@PutMapping("{/id}")
	public ResponseEntity<Calificaciones> actualizar(@PathVariable Integer id, @Valid @RequestBody Calificaciones calificaciones) {
		return new ResponseEntity<>(calificacionesService.actualizar(calificaciones), HttpStatus.ACCEPTED);
	}
	@DeleteMapping("{/id}")
	public ResponseEntity<Response> eliminar(@PathVariable Integer id) {
		calificacionesService.eliminar(id);
		return new ResponseEntity<>(new Response(), HttpStatus.OK);
	}
}
