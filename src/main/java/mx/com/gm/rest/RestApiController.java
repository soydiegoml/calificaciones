package mx.com.gm.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.gm.domain.Alumnos;
import mx.com.gm.domain.MateriasAlumnos;
import mx.com.gm.service.AlumnosService;
import mx.com.gm.service.MateriasAlumnosService;

@RestController
@RequestMapping("v1/alumnos")
public class RestApiController {

	@Autowired
	private AlumnosService alumnosService;
	
	@Autowired
	private MateriasAlumnosService materiasAlumnosService;
	
//	@GetMapping
//	public List<Alumnos> getAll(){
//		return alumnosService.listarAlumnos();
//	}
	
	@GetMapping
	public List<MateriasAlumnos> getAllMateriasAlumnos(){
		return materiasAlumnosService.listarMateriasAlumnos();
	}
	
	@PostMapping
	public ResponseEntity<Alumnos> registrar(@Valid @RequestBody Alumnos alumnos) {
		return new ResponseEntity<Alumnos>(alumnosService.registrar(alumnos), HttpStatus.CREATED);
	}
}
