package mx.com.gm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.gm.domain.Calificaciones;

public interface CalificacionesDao extends JpaRepository<Calificaciones, Long>{

}
