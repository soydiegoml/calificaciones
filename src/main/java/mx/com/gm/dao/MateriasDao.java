package mx.com.gm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.gm.domain.Materias;

public interface MateriasDao extends JpaRepository<Materias, Long> {
	
}
