package mx.com.gm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.com.gm.domain.Alumnos;

public interface AlumnosDao extends JpaRepository<Alumnos, Long> {
	
	@Query(value = "SELECT a.id, a.nombre, a.apellido_paterno, am.id_materia FROM alumnos as a INNER JOIN alumnos_materias as am ON alumnos.id = alumnos_materias.id_alumno", nativeQuery = true)
	public List<Alumnos> verAlumnos();
}
