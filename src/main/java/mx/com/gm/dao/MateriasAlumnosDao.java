package mx.com.gm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.gm.domain.MateriasAlumnos;


public interface MateriasAlumnosDao extends JpaRepository<MateriasAlumnos, Long>{
	
}
