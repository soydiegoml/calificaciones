package mx.com.gm.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name="materias")
public class Materias {
	@Id
	@GeneratedValue
	private int id_materia;
	@NotNull
	private String materia;
}
