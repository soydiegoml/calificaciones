package mx.com.gm.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name="alumnos_materias")
public class MateriasAlumnos {
	@Id
	@GeneratedValue
	private int id;
	@NotNull
	private int id_alumno;
	@NotNull
	private int id_materia;
	
	@ManyToOne
	@JoinColumn(name = "id_materia", insertable = false, updatable = false)
	Materias materias;
}
