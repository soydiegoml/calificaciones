package mx.com.gm.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name="alumnos_materias_calificaciones")
public class Calificaciones {
	@Id
	@GeneratedValue
	private int id;
	@NotNull
	private int id_alumnos_materias;
	@NotNull
	private int calificacion;
}
