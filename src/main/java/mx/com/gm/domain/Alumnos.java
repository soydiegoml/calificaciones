package mx.com.gm.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name="alumnos")
public class Alumnos {
	@Id
	@GeneratedValue
	private int id;
	@NotEmpty(message = "Este campo es obligatorio")
	private String nombre;
	@NotNull(message = "Este campo es obligatorio")
	private String apellidoPaterno;
	@NotNull(message = "Este campo es obligatorio")
	private String apellidoMaterno;
//	@ManyToOne
//	@JoinColumn(name = "id")
//	Materias materias;
}
