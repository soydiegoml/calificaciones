package mx.com.gm.service;

import java.util.List;

import mx.com.gm.domain.MateriasAlumnos;

public interface MateriasAlumnosService {
	public List<MateriasAlumnos> listarMateriasAlumnos();
	
	public MateriasAlumnos registrar(MateriasAlumnos materiasAlumnos);
    
    public void guardar(MateriasAlumnos materiasAlumnos);
    
    public void eliminar(MateriasAlumnos materiasAlumnos);
    
    public MateriasAlumnos encontrarPersona(MateriasAlumnos materiasAlumnos);

}
