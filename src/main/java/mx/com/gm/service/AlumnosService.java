package mx.com.gm.service;

import java.util.List;

import mx.com.gm.domain.Alumnos;

public interface AlumnosService {
	public List<Alumnos> listarAlumnos();
	
	public Alumnos registrar(Alumnos alumnos);
    
    public void guardar(Alumnos alumnos);
    
    public void eliminar(Alumnos alumnos);
    
    public Alumnos encontrarPersona(Alumnos alumnos);
}
