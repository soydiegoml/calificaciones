package mx.com.gm.service;

import java.util.List;

import mx.com.gm.domain.Materias;

public interface MateriasService {
	public List<Materias> listarAlumnos();
	
	public Materias registrar(Materias materias);
    
    public void guardar(Materias materias);
    
    public void eliminar(Materias materias);
    
    public Materias encontrarPersona(Materias materias);
}
