package mx.com.gm.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.dao.MateriasAlumnosDao;
import mx.com.gm.domain.MateriasAlumnos;

@Service
public class MateriasAlumnosServiceImpl implements MateriasAlumnosService{

	 	@Autowired
	    private MateriasAlumnosDao materiasAlumnosDao;
	    
		@Override
	    @Transactional(readOnly = true)
	    public List<MateriasAlumnos> listarMateriasAlumnos() {
	        return (List<MateriasAlumnos>) materiasAlumnosDao.findAll();
	    }

	    @Override
	    @Transactional
	    public void guardar(MateriasAlumnos materiasAlumnos) {
	    	materiasAlumnosDao.save(materiasAlumnos);
	    }

	    @Override
	    @Transactional
	    public void eliminar(MateriasAlumnos materiasAlumnos) {
	    	materiasAlumnosDao.delete(materiasAlumnos);
	    }

		@Override
		public MateriasAlumnos encontrarPersona(MateriasAlumnos materiasAlumnos) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public MateriasAlumnos registrar(MateriasAlumnos materiasAlumnos) {
			// TODO Auto-generated method stub
			return materiasAlumnosDao.save(materiasAlumnos);
		}


}