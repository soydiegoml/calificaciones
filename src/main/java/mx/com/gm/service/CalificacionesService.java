package mx.com.gm.service;

import java.util.List;

import javax.validation.Valid;

import mx.com.gm.domain.Calificaciones;

public interface CalificacionesService {

	public List<Calificaciones> listarCalificaciones();
	
	//public Calificaciones Calificaciones(Calificaciones calificaciones);
    
    public void guardar(Calificaciones calificaciones);
    
    public void eliminar(Integer id);
    
    public Calificaciones encontrarPersona(Calificaciones calificaciones);

	public Calificaciones registrar(Calificaciones calificaciones);

	public Calificaciones actualizar(@Valid Calificaciones calificaciones);

	public void eliminar(Calificaciones calificaciones);
}
