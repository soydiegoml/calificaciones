package mx.com.gm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.dao.CalificacionesDao;
import mx.com.gm.domain.Calificaciones;

@Service
public class CalificacionesServiceImpl implements CalificacionesService {
	
	@Autowired
	private CalificacionesDao calificacionesDao;

	@Override
    @Transactional(readOnly = true)
    public List<Calificaciones> listarCalificaciones() {
        return (List<Calificaciones>) calificacionesDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Calificaciones calificaciones) {
    	calificacionesDao.save(calificaciones);
    }

    @Override
    @Transactional
    public void eliminar(Calificaciones calificaciones) {
    	calificacionesDao.delete(calificaciones);
    }

	@Override
	public Calificaciones encontrarPersona(Calificaciones calificaciones) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Calificaciones registrar(Calificaciones calificaciones) {
		// TODO Auto-generated method stub
		return calificacionesDao.save(calificaciones);
	}
	
	@Override
	public Calificaciones actualizar(Calificaciones calificaciones) {
		// TODO Auto-generated method stub
		return calificacionesDao.save(calificaciones);
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		
	}
	



}
