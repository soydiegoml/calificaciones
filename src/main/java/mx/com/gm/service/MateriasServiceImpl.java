package mx.com.gm.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.dao.MateriasDao;
import mx.com.gm.domain.Materias;

@Service
public class MateriasServiceImpl implements MateriasService{

	 	@Autowired
	    private MateriasDao materiasDao;
	    
		@Override
	    @Transactional(readOnly = true)
	    public List<Materias> listarAlumnos() {
	        return (List<Materias>) materiasDao.findAll();
	    }

	    @Override
	    @Transactional
	    public void guardar(Materias materias) {
	        materiasDao.save(materias);
	    }

	    @Override
	    @Transactional
	    public void eliminar(Materias materias) {
	    	materiasDao.delete(materias);
	    }

		@Override
		public Materias encontrarPersona(Materias materias) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Materias registrar(Materias materias) {
			// TODO Auto-generated method stub
			return materiasDao.save(materias);
		}

}
