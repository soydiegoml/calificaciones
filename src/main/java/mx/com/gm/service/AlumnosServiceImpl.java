package mx.com.gm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.dao.AlumnosDao;
import mx.com.gm.domain.Alumnos;

@Service
public class AlumnosServiceImpl implements AlumnosService{

	 	@Autowired
	    private AlumnosDao alumnosDao;
	    
		@Override
	    @Transactional(readOnly = true)
	    public List<Alumnos> listarAlumnos() {
	        return (List<Alumnos>) alumnosDao.findAll();
	    }

	    @Override
	    @Transactional
	    public void guardar(Alumnos alumnos) {
	        alumnosDao.save(alumnos);
	    }

	    @Override
	    @Transactional
	    public void eliminar(Alumnos alumnos) {
	        alumnosDao.delete(alumnos);
	    }

		@Override
		public Alumnos encontrarPersona(Alumnos alumnos) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Alumnos registrar(Alumnos alumnos) {
			// TODO Auto-generated method stub
			return alumnosDao.save(alumnos);
		}

//	    @Override
//	    @Transactional(readOnly = true)
//	    public Alumnos encontrarPersona(Alumnos alumnos) {
//	        return alumnosDao.findById(persona.getIdPersona()).orElse(null);
//	    }
}
