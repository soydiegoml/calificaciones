CREATE TABLE alumnos (
	id int NOT NULL AUTO_INCREMENT, 
	PRIMARY KEY (id),
	nombre varchar(100) NOT NULL,
	apellido_paterno varchar(50) NOT NULL,
	apellido_materno varchar(50) NOT NULL
);
CREATE TABLE materias (
	id_materia int NOT NULL AUTO_INCREMENT, 
	PRIMARY KEY (id_materia),
	materia varchar(100) NOT NULL,
	FOREIGN KEY (id_materia) REFERENCES alumnos_materias(id_materia)
);
CREATE TABLE alumnos_materias (
	id int NOT NULL AUTO_INCREMENT, 
	id_alumno int NOT NULL,
	id_materia int NOT NULL, 
	PRIMARY KEY (id),
	FOREIGN KEY (id_alumno) REFERENCES alumnos(id)
);
CREATE TABLE alumnos_materias_calificaciones (
	id int NOT NULL AUTO_INCREMENT, 
	id_alumnos_materias int not null,
	calificacion int not null,
	PRIMARY KEY (id)
	
);
